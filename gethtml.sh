#!/bin/bash

PANE=$(cat .panenumber)

# Count from 1 to 10 with a sleep
while true; do
    tmux capture-pane -eJ -t $PANE -p | ansi2html
    echo "EOF EOF EOF"
    sleep $1
done
