FROM debian:testing-slim AS builder

WORKDIR /root

RUN apt update && \
    apt install -y wget libncurses-dev build-essential pkg-config \
    && rm -rf /var/lib/apt/lists/*

RUN wget https://gitlab.com/SyocR/cbonsai/-/archive/master/cbonsai-master.tar.gz && \
    tar -xf cbonsai-master.tar.gz

WORKDIR /root/cbonsai-master

RUN make install PREFIX=/root/





FROM debian:testing-slim

WORKDIR /root

COPY --from=builder /root/bin/cbonsai cbonsai

RUN apt update && \
    apt install -y tmux wget colorized-logs libncurses-dev

RUN wget -q https://github.com/joewalnes/websocketd/releases/download/v0.4.1/websocketd-0.4.1_amd64.deb && \
    dpkg -i websocketd-0.4.1_amd64.deb

COPY gethtml.sh gethtml.sh
COPY start.sh start.sh

EXPOSE 8080

ENTRYPOINT ["./start.sh"]
