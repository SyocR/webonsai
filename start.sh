#!/bin/sh

SLEEP=1

if [ "$1" = "--sleep" ]; then
    SLEEP=$2
fi

tmux new-session -d -s cbonsai-web
tmux send-keys -t 0 'echo $TMUX_PANE > .panenumber' C-m \
    'sleep 0.5' C-m \
    "./cbonsai -l -i -t $SLEEP" C-m

tmux new-window -t cbonsai-web:1
tmux send-keys -t 1 "websocketd --port=8080 ./gethtml.sh $SLEEP" C-m

tmux -t cbonsai-web select-window -t 1
tmux attach -t cbonsai-web
